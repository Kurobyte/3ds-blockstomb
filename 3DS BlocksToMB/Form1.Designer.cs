﻿namespace _3DS_BlocksToMB
{
    partial class frmMain
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.txtBlock = new System.Windows.Forms.TextBox();
            this.btnConvert = new System.Windows.Forms.Button();
            this.lblBlocks = new System.Windows.Forms.Label();
            this.grbOptions = new System.Windows.Forms.GroupBox();
            this.rbGBtoBlock = new System.Windows.Forms.RadioButton();
            this.rbMBtoBlock = new System.Windows.Forms.RadioButton();
            this.rbBlockToMB = new System.Windows.Forms.RadioButton();
            this.lblKuro = new System.Windows.Forms.Label();
            this.grbOptions.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtBlock
            // 
            this.txtBlock.Location = new System.Drawing.Point(24, 25);
            this.txtBlock.Name = "txtBlock";
            this.txtBlock.Size = new System.Drawing.Size(166, 20);
            this.txtBlock.TabIndex = 0;
            // 
            // btnConvert
            // 
            this.btnConvert.Location = new System.Drawing.Point(200, 23);
            this.btnConvert.Name = "btnConvert";
            this.btnConvert.Size = new System.Drawing.Size(75, 23);
            this.btnConvert.TabIndex = 1;
            this.btnConvert.Text = "Convert";
            this.btnConvert.UseVisualStyleBackColor = true;
            this.btnConvert.Click += new System.EventHandler(this.btnConvert_Click);
            // 
            // lblBlocks
            // 
            this.lblBlocks.AutoSize = true;
            this.lblBlocks.Location = new System.Drawing.Point(21, 9);
            this.lblBlocks.Name = "lblBlocks";
            this.lblBlocks.Size = new System.Drawing.Size(42, 13);
            this.lblBlocks.TabIndex = 2;
            this.lblBlocks.Text = "Blocks:";
            // 
            // grbOptions
            // 
            this.grbOptions.Controls.Add(this.rbGBtoBlock);
            this.grbOptions.Controls.Add(this.rbMBtoBlock);
            this.grbOptions.Controls.Add(this.rbBlockToMB);
            this.grbOptions.Location = new System.Drawing.Point(12, 51);
            this.grbOptions.Name = "grbOptions";
            this.grbOptions.Size = new System.Drawing.Size(276, 47);
            this.grbOptions.TabIndex = 3;
            this.grbOptions.TabStop = false;
            this.grbOptions.Text = "Options";
            // 
            // rbGBtoBlock
            // 
            this.rbGBtoBlock.AutoSize = true;
            this.rbGBtoBlock.Location = new System.Drawing.Point(181, 19);
            this.rbGBtoBlock.Name = "rbGBtoBlock";
            this.rbGBtoBlock.Size = new System.Drawing.Size(82, 17);
            this.rbGBtoBlock.TabIndex = 2;
            this.rbGBtoBlock.TabStop = true;
            this.rbGBtoBlock.Text = "GB to Block";
            this.rbGBtoBlock.UseVisualStyleBackColor = true;
            this.rbGBtoBlock.CheckedChanged += new System.EventHandler(this.rbGBtoBlock_CheckedChanged);
            // 
            // rbMBtoBlock
            // 
            this.rbMBtoBlock.AutoSize = true;
            this.rbMBtoBlock.Location = new System.Drawing.Point(95, 19);
            this.rbMBtoBlock.Name = "rbMBtoBlock";
            this.rbMBtoBlock.Size = new System.Drawing.Size(83, 17);
            this.rbMBtoBlock.TabIndex = 1;
            this.rbMBtoBlock.TabStop = true;
            this.rbMBtoBlock.Text = "MB to Block";
            this.rbMBtoBlock.UseVisualStyleBackColor = true;
            this.rbMBtoBlock.CheckedChanged += new System.EventHandler(this.rbMBtoBlock_CheckedChanged);
            // 
            // rbBlockToMB
            // 
            this.rbBlockToMB.AutoSize = true;
            this.rbBlockToMB.Location = new System.Drawing.Point(6, 19);
            this.rbBlockToMB.Name = "rbBlockToMB";
            this.rbBlockToMB.Size = new System.Drawing.Size(83, 17);
            this.rbBlockToMB.TabIndex = 0;
            this.rbBlockToMB.TabStop = true;
            this.rbBlockToMB.Text = "Block to MB";
            this.rbBlockToMB.UseVisualStyleBackColor = true;
            this.rbBlockToMB.CheckedChanged += new System.EventHandler(this.rbBlockToMB_CheckedChanged);
            // 
            // lblKuro
            // 
            this.lblKuro.AutoSize = true;
            this.lblKuro.ForeColor = System.Drawing.Color.DimGray;
            this.lblKuro.Location = new System.Drawing.Point(239, 98);
            this.lblKuro.Name = "lblKuro";
            this.lblKuro.Size = new System.Drawing.Size(63, 13);
            this.lblKuro.TabIndex = 4;
            this.lblKuro.Text = "by Kurobyte";
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(300, 113);
            this.Controls.Add(this.lblKuro);
            this.Controls.Add(this.grbOptions);
            this.Controls.Add(this.lblBlocks);
            this.Controls.Add(this.btnConvert);
            this.Controls.Add(this.txtBlock);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "frmMain";
            this.Text = "3DS Block to MB";
            this.grbOptions.ResumeLayout(false);
            this.grbOptions.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtBlock;
        private System.Windows.Forms.Button btnConvert;
        private System.Windows.Forms.Label lblBlocks;
        private System.Windows.Forms.GroupBox grbOptions;
        private System.Windows.Forms.RadioButton rbMBtoBlock;
        private System.Windows.Forms.RadioButton rbBlockToMB;
        private System.Windows.Forms.RadioButton rbGBtoBlock;
        private System.Windows.Forms.Label lblKuro;
    }
}

