﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _3DS_BlocksToMB
{
    public partial class frmMain : Form
    {
        string[] unidades = { "MB", "GB" };
        public frmMain()
        {
            InitializeComponent();
            rbBlockToMB.Checked = true;
        }

        private void btnConvert_Click(object sender, EventArgs e)
        {
            int idataSize;
            double dataSize;

            try
            {

                if (rbBlockToMB.Checked)
                {
                    if (int.TryParse(txtBlock.Text, out idataSize))
                        BlockToMB(idataSize);
                    else
                        MessageBox.Show("Hey buddy insert valid numbers.\nDon't use special characters like commas if you are converting Blocks to MB.", "Advertence", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else if (rbMBtoBlock.Checked)
                {
                    if (double.TryParse(txtBlock.Text.Replace('.',','), out dataSize))
                        MBtoBlock(dataSize);
                    else
                        MessageBox.Show("Hey buddy insert valid numbers.", "Advertence", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    if (double.TryParse(txtBlock.Text.Replace('.', ','), out dataSize))
                        MBtoBlock(dataSize * 1024);
                    else
                        MessageBox.Show("Hey buddy insert valid numbers.", "Advertence", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }   
            }
            catch
            {
                MessageBox.Show("Are you sure that exist a game in the eShop with \""+txtBlock.Text+"\" blocks?\nPlease insert the correct block number", "Advertence", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void MBtoBlock(double dataSize)
        {
            int dataBlock = 0;
            int unit = 0;
            double txtSize = dataSize;
            if (dataSize >= 1024)
            {
                unit++;
                txtSize = Math.Round(txtSize / 1024,2);
                dataBlock = (int)Math.Floor(dataSize * 8);
            }
            else
            {
                dataBlock = (int)Math.Floor(dataSize * 8);
            }
                MessageBox.Show(txtSize + unidades[unit]+ " are " + dataBlock +" Blocks", "Process complete!", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        public void BlockToMB(int dataSize)
        {
            double dataMB;
            int unit = 0;

            if (dataSize >= 8192)
            {
                dataMB = Math.Round(((double)dataSize / 8) / 1024,2);
                unit++;

            }
            else
            {
                dataMB = dataSize / 8;
            }


            MessageBox.Show(txtBlock.Text + " blocks are " + dataMB + unidades[unit], "Process complete!", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void rbBlockToMB_CheckedChanged(object sender, EventArgs e)
        {
            lblBlocks.Text = "Blocks:";
        }

        private void rbMBtoBlock_CheckedChanged(object sender, EventArgs e)
        {
            lblBlocks.Text = "MB:";
        }

        private void rbGBtoBlock_CheckedChanged(object sender, EventArgs e)
        {
            lblBlocks.Text = "GB:";
        }

    }
}
